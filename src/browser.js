const api = require("./api");
const auth = require("./auth");
const broadcast = require("./broadcast");
const config = require("./config");
const formatter = require("./formatter")(api);
const utils = require("./utils");

const wlsjs = {
  api,
  auth,
  broadcast,
  config,
  formatter,
  utils
};

if (typeof window !== "undefined") {
  window.wlsjs = wlsjs;
}

if (typeof global !== "undefined") {
  global.wlsjs = wlsjs;
}

exports = module.exports = wlsjs;
