var ChainTypes;

module.exports = ChainTypes = {};

ChainTypes.reserved_spaces = {
  relative_protocol_ids: 0,
  protocol_ids: 1,
  implementation_ids: 2
};

ChainTypes.operations= {
    vote: 0,
    comment: 1,
    transfer: 2,
    transfer_to_vesting: 3,
    withdraw_vesting: 4,
    account_create: 5,
    account_update: 6,
    account_forsale: 7,
    account_buying: 8,
    witness_update: 9,
    account_witness_vote: 10,
    account_witness_proxy: 11,
    custom: 12,
    delete_comment: 13,
    custom_json: 14,
    comment_options: 15,
    set_withdraw_vesting_route: 16,
    custom_binary: 17,
    claim_reward_balance: 18,
    author_reward: 19, /// virtual operations from here
    curation_reward: 20,
    comment_reward: 21,
    fill_vesting_withdraw: 22,
    shutdown_witness: 23,
    hardfork: 24,
    comment_payout_update: 25,
    comment_benefactor_reward: 26
};

//types.hpp
ChainTypes.object_type = {
  "null": 0,
  base: 1,
};
